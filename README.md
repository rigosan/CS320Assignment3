prog3_1:
Driver that simply creates a lua state and runs the file from command line.


prog3_2:
A lua file containing the InfixtoPostfix function.

prog3_3:
Driver that creates lua state and runs prog3_2.lua and pass a string from stdin into the InfixtoPostfix function, returning the resulting string in postfix and printing it.


