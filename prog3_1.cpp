#include <lua.hpp>
#include <lauxlib.h>
#include <lualib.h>
#include <stdlib.h>
#include <stdio.h>

int main(int argc, char** argv){
    printf("Assignment #3-1, Sergio Santana, rigosanle@gmail.com\n");
    lua_State *L; 
    L=luaL_newstate();
    luaL_openlibs(L);
    //***above is necessary stuff for all lua links
    char* filetoLoad=argv[1];
    luaL_loadfile(L,filetoLoad); //loads the file
    //printf("IN C, calling lua.\n");  
     
    lua_pcall(L,0,0,0); //RUN THE PRIME CALL    
    //lua_getglobal(L,"tellme");//PUSH TELLME ONTO lUA STACK
    //lua_pcall(L,0,0,0); //call tell me
    //printf("bACK IN c.\n"); 
    
    //lua_getglobal(L,"InfixToPostfix");
    //lua_pushstring(L,"3 + 5 * 6 / 2 + 1");
    //lua_pcall(L,1,1,0); //L,#of args,#of returns,#idx)
    //const char* mynum=lua_tostring(L,-1);
    //printf("Returned number=%s\n",mynum);
    //lua_close(L);
    return 0;


    
}
