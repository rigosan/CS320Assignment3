operands="123456789"
operators="+/%*-"
print("Assignment #3-3, Sergio Santana, rigosanle@gmail.com")
function InfixToPostfix(str)
  
    --tokenize str
    --return in PostFix
    tokens={}
    lenTok=0
    output=""
    stack={}
    lenStack=0
    
    --obtain table of tokens
    for token in str:gmatch("%S+") do
        table.insert(tokens,token)
        lenTok=lenTok+1
    end
    
     for s=1,lenTok do
        tempS=tokens[s]
        
        if not string.match(operators,tempS) then
            
            output=output.." "..tempS
            
        else 
            topStack=stack[lenStack]
            if OperandVal(tempS)>OperandVal(topStack) or lenStack==0 then
                table.insert(stack,tempS)
                lenStack=lenStack+1
             else
                
                topStack=stack[lenStack]
                
                while(OperandVal(tempS)<=OperandVal(topStack) and lenStack~=0) do
                    output=output.." "..table.remove(stack)
                    lenStack=lenStack-1
                    topStack=stack[lenStack]
                end
                table.insert(stack,tempS)
                lenStack=lenStack+1  
             
            end 
                
        
        end
    end
   
    for x=1,lenStack do
        popped=table.remove(stack)
        output=output.." "..popped
    end
                        
            
    

    
    return (output)
    
end



function OperandVal(str)
    val=0
    if str=="+" or str=="-" then
        val=1
    else 
        val=2
    end    
    return (val)
end
